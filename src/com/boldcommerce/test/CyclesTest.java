package com.boldcommerce.test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.boldcommerce.project.Cycles;

class CyclesTest {

	@Test
	void nextMonthlyCycle() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2017-jan-15", format);
		LocalDate givenDate = LocalDate.parse("2017-jan-19", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2017-fev-15", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}
	
	@Test
	void nextMonthlyCycleOneDayAfterStartDate() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2018-dez-19", format);
		LocalDate givenDate = LocalDate.parse("2018-dez-20", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2019-jan-19", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}	
	
	@Test
	void nextMonthlyCycleWhenGivenDateisBeforeStartDate() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2018-jun-16", format);
		LocalDate givenDate = LocalDate.parse("2018-jun-13", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2018-jul-16", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}

	@Test
	void nextMonthlyCycleWhenGivenDateisEqualStartDate() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2018-jun-16", format);
		LocalDate givenDate = LocalDate.parse("2018-jun-13", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2018-jul-16", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}

	@Test
	void nextMonthlyCycleWrongLeapYear() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2017-fev-29", format);
		LocalDate givenDate = LocalDate.parse("2017-mar-01", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2017-mar-28", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}

	@Test
	void nextMonthlyCycleRightLeapYear() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
		LocalDate startDate = LocalDate.parse("2016-fev-29", format);
		LocalDate givenDate = LocalDate.parse("2016-mar-01", format);

		Cycles cycle = new Cycles();
		Assertions.assertEquals(LocalDate.parse("2016-mar-29", format), cycle.nextMonthlyCycle(startDate, givenDate));
	}	

}
