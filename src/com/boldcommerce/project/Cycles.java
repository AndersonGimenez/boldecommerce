package com.boldcommerce.project;

import java.time.LocalDate;

public class Cycles {

	public LocalDate nextMonthlyCycle(LocalDate startDate, LocalDate givenDate) {
		LocalDate date;

		/*
		 * Once you have one given date less than a start date you can assume
		 * that is the same as start date instead throw out an error
		 */
		if (givenDate.isBefore(startDate)) {
			givenDate = startDate;
		}

		/*
		 * Once I have the same date given equals the same start date, I can avoid the
		 * loop and return one month directly
		 */
		if (startDate.equals(givenDate)) {
			return startDate.plusMonths(1);
		}

		/*
		 * Looping every month to add one in the end
		 */
		for (date = startDate; date.isBefore(givenDate); date.plusMonths(1)) {
			date = date.plusMonths(1);
		}
		return date;
	}
}
